<?php

// ===============Main Activity=====================//
class Building {

	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name,$floors,$address){

		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;

	}

    //For Name
    public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	
	}

    //For Floors
	public function getFloors(){
        return $this->floors;
    }

    
    //for instruction number 2, I hope it's correct.
    private function setFloors($floors){
        $this->floors = $floors;
    }

    //For Address
    public function getAddress(){
        return $this->address;
    }

    //for instruction number 2, I hope it's correct.
    private function setAddress($address){
        $this->address = $address;
    }

}


class Condominium extends Building{

    //I just moved the getter and setter (from instruction 1) that I created here to the Building class, as per instruction number 3..
    
}

$building = new Building('Caswynn Building',8,'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo',5,'Buendia Avenue, Makati City, Philippines');


// ===============Supp-Act=====================//

$tasks = ["Get Git", "Bake HTML", "Eat CSS", "Learn PHP"];

    if(isset($_GET["index"])){
        $indexGet = $_GET["index"];
        echo "The retrieved task from GET is {$tasks[$indexGet]}. <br>";
    }

    if(isset($_POST["index"])){
        $indexPost = $_POST["index"];
        echo "The retrieved task from POST is {$tasks[$indexPost]}. <br>";
    }