<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity s04</title>
</head>
<body>

<!-- ================Supp-Act========================== -->
<h1>Task index from GET</h1>
<form method="GET">
    <select name="index" required>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
    </select>

    <button type="submit">GET</button>
</form>

<h1>Task index from POST</h1>
<form method="POST">
    <select name="index" required>
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
    </select>

    <button type="submit">POST</button>
</form>
<hr>
<hr>

<!-- ===========Revised Main Activity============ -->
<h1>Building</h1>
	<p>The name of the building is <?php echo $building->getName();?>.</p>
	<p>The <?php echo $building->getName();?> has <?php echo $building->getFloors();?> .</p>
	<p>The <?php echo $building->getName();?> is lcoated at <?php echo $building->getAddress();?> .</p>
	<p>The name of the building has been changed to <?php echo $building->setName("Caswynn Complex");?><?php echo $building->getName();?>.</p>

	<h1>Condominium</h1>
    <p>The name of the condominium is <?php echo $condominium->getName();?>.</p>
	<p>The <?php echo $condominium->getName();?> has <?php echo $condominium->getFloors();?> .</p>
	<p>The <?php echo $condominium->getName();?> is lcoated at <?php echo $condominium->getAddress();?> .</p>
	<p>The name of the condominium has been changed to <?php echo $condominium->setName("Caswynn Complex");?><?php echo $building->getName();?>.</p>
    
</body>
</html>